**Advantages and Disadvantages of owning a Cast Iron Cookware**

Cast iron is a type of iron which is melted on a temperature of 2800F and then moulded in different shapes and sizes using sand moulds.
No doubt, a cook is an artist and his/her cookware are the brushes but as every artist has different taste in the types of brushes they are using, every cook would also want cooking utensils that are the most handy. Here are some of the advantages and disadvantages to acknowledge before you buy a cast iron cookware.

**• Advantages**

Cast iron cookware is ideal for both gas stoves and induction stoves. You just need a heat source and that’s it. Which means you can use it during camping or other outside occasions on coal or wood stoves, making it a good choice for survival cooking during power cut outs. Furthermore, iron-cast cookware will last for generations if you take good care of it. This makes it cost effective too. You can also make a ritual of passing it on to your kids and your kids will pass them on to theirs, and so on (this already happens in many family oriented people). Cast-iron cookware is proved to be healthier scientifically than cookware of other material such as Teflon which is also said to be one of the reasons of cancer. Another heavenly aspect of it includes its versatility as discussed above too. When cooking recipes requiring stove heat and for a mild finish into the oven for a couple of minutes, this will be quite easier using cast-iron cookware which will be suitable for both platforms and you don’t even need to change pans!

**• Disadvantages**

Cast-iron cookware sure needs high maintenance. You can’t just put food in it and place it on the stove, you first need to season it properly. You cannot put it in a dishwasher, air dry it or even use SOS pads to wash them. A stiffed-bristled brush is what you need! If you’re using bare cast-iron cookware you should stick to wooden or bamboo spoons and spatulas being bad conductors of heat, so you don’t burn your hand. If using enamelled cast-iron cookware, do not use them with high flame or heat because this might damage the coated surface. Also they are made for indoor cooking and not for camp fire or barbecuing outdoors.
I hope this helped you make your mind, whether to buy a [cast iron cookware](https://www.fromrussia.com/home-goods/kitchen/cookware/cast-iron-cookware) or not and if you own it already, if you should consider keeping it or not.

